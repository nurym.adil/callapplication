﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CallApplication
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        public UdpClient UdpClient { get; set; } = new UdpClient(5001);
        public UdpClient UdpListener { get; set; } = new UdpClient(5002);
        public WaveIn Input { get; set; }
        public WaveOut Output { get; set; }
        public BufferedWaveProvider BufferStream { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            Input = new WaveIn();
            Output = new WaveOut();

            Input.WaveFormat = new WaveFormat(8000, 16, 1);
            Input.DataAvailable += VoiceInput;
            BufferStream = new BufferedWaveProvider(new WaveFormat(8000, 16, 1));
            Output.Init(BufferStream);

            Task task = Task.Run((Action)Listening);
        }
        private void Listening()
        {
            Output.Play();
            UdpListener.BeginReceive(Receive, null);
        }

        private void Receive(IAsyncResult ar)
        {
            IPEndPoint remoteIp = null;
            byte[] data = UdpListener.EndReceive(ar, ref remoteIp);
            BufferStream.AddSamples(data, 0, data.Length);
            UdpListener.BeginReceive(Receive, null);
        }

        private async void VoiceInput(object sender, WaveInEventArgs e)
        {
            IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Parse(addressTextBox.Text), 5002);
            await UdpClient.SendAsync(e.Buffer, e.Buffer.Length, remoteEndPoint);

        }
        private void MakeCall(object sender, RoutedEventArgs e)
        {
            Input.StartRecording();
        }

    }
}

